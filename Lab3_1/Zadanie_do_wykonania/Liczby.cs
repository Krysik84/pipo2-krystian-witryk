﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie_do_wykonania
{
    public class Liczby
    {
        private int a;
        private int b;
        private int c;

        public Liczby()
        {
            this.a = 0;
            this.b = 0;
            this.c = 0;
        }

        public void set_a(int a)
        {
            this.a = a;
        }

        public void set_b(int b)
        {
            this.b = b;
        }

        public void set_c(int c)
        {
            this.c = c;
        }

        public int get_a()
        {
            return this.a;
        }

        public int get_b()
        {
            return this.b;
        }

        public int get_c()
        {
            return this.c;
        }




    }
}