﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void silnia_0()
        {
            silnia s = new silnia();
            s.n = 0;
            Assert.AreEqual(s.oblicz(), 1);
        }
        [TestMethod]
        public void silnia_1()
        {
            silnia s = new silnia();
            s.n = 1;
            Assert.AreEqual(s.oblicz(), 1);
        }
        [TestMethod]
        public void silnia_5()
        {
            silnia s = new silnia();
            s.n = 5;
            Assert.AreEqual(s.oblicz(), 120);
        }
    }
}
